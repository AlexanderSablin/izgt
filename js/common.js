$(function() {
	var left = $('.spinner1').css('left');
	var loop = false;

	if ($(window).width() <= '995'){
		$('.flex-row .box:first-child').toggleClass('active');
		$('.spinner1').css({'animation':'mymove 4s linear infinite'});
		myVar = setTimeout(function(){
			$('.spinner2').css({'animation':'mymove3 4s linear infinite'});
			$('.spinner3').css({'animation':'mymove3 4s linear infinite'});
			$('.spinner4').css({'animation':'mymove 4s linear infinite'});
		},3000)
	} else {

		$('.flex-row .box:first-child').mouseenter(function(){
			$(this).toggleClass('active');
			$('.spinner1').css({'animation':'mymove 4s linear infinite'});
			myVar = setTimeout(function(){
				$('.spinner2').css({'animation':'mymove3 4s linear infinite'});
				$('.spinner3').css({'animation':'mymove3 4s linear infinite'});
				$('.spinner4').css({'animation':'mymove 4s linear infinite'});
			},3000)
		});
		$('.flex-row .box:first-child').mouseleave(function(){
			clearTimeout(myVar);
			$(this).toggleClass('active');
			$('.spinner1').css({'animation':''});
			$('.spinner2').css({'animation':''});
			$('.spinner3').css({'animation':''});
			$('.spinner4').css({'animation':''});
		});
	}

	
	var figure = $('.video').hover( hoverVideo, hideVideo );
	function hoverVideo(e) {
		$('.video video', this).css('opacity', '1');
	  	$('.video video')[0].play();
	}
	function hideVideo(e) {
		$('.video video')[0].pause();
	};
	
	$('.flex-row .box').mouseenter(function(){
		$(this).find('.circle').find('span:nth-child(1)').css({
			'animation':'pulse-me 3s linear infinite'
		});
		$(this).find('.circle').find('span:nth-child(2)').css({
			'animation':'pulse-me 3s linear infinite'
		});
		$(this).find('.circle').find('span:nth-child(3)').css({
			'animation':'pulse-me 3s linear infinite'
		});
	});
	$('.flex-row .box').mouseleave(function(){
		$(this).find('.circle').find('span').attr('style','');
	});
	$('select').niceSelect();
	$('.hamburger').click(function(){
		$('.mobile-menu').toggleClass('active');
		$('html,body').toggleClass('active');
	})
	$('.mobile-menu .close').click(function(){
		$('.mobile-menu').toggleClass('active');
		$('html,body').toggleClass('active');
	});

		$(".owl-carousel").owlCarousel({
			items:1,
			loop: true,
			margin:0,
			stagePadding:0,
			smartSpeed:450,
			autoplay:true,
			autoplayTimeout:3000,
			autoplayHoverPause:true
		});


		var a = 0, b = 0, i = 0;
		$('.peoples').viewportChecker({
			callbackFunction: function(elem, action){
				var blockNum = document.getElementById('numBlock');
				var blockNum2 = document.getElementById('numBlock2');
				var blockNum3 = document.getElementById('numBlock3');
				setInterval(function(){
					if(i<258) {
						i += 1;
						blockNum.innerHTML = i;
					};
					if(a<17) {
						a += 1;
						blockNum2.innerHTML = a;
					};
					if(b<118) {
						b += 1;
						blockNum3.innerHTML = b;
					};
				},10);
			}});

	$('.about-page__square-btn:not(.fancybox_video)').click(function(e){
		e.preventDefault();
		$(this).parent('.about-page__square').addClass('about-page__square_show');
	});

	$('.about-page__square-btn-close').click(function(){
		$(this).parent('.about-page__square').removeClass('about-page__square_show');
	});

	$('.peoples').mousemove(function(e) {
		var change;
		var xpos=e.clientX;var ypos=e.clientY;var left= change*20;
		var  xpos=xpos*2;ypos=ypos*2;
		$('.spaceman').css('top',((0+(ypos/50))+"px"));
		$('.spaceman').css('right',(( 0+(xpos/80))+"px"));

	});
});

